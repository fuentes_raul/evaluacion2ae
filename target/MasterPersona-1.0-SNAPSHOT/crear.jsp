<%-- 
    Document   : editar
    Created on : 19-04-2020, 18:48:25
    Author     : ANDRES
--%>

<%@page import="root.model.entities.MasterPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body  class="text-center">
        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            <header class="mast mb-auto">
                <div class="inner">
                </div>
            </header>
        </div>   
        <main role="main" class="inner cover">
            <h2 class="cover-heading"></h2>
              <h2>Crear:</h2>
            <p class="">
              
            <form name="form" action="controllerPersona" method="POST">
                <div class="form-group">
                    <label for="identificacion">Identificacion: </label><br>
                    <input name="identificacion" class="form-control" required id="identificacion" aria-describedby="indentificacionHelp" />
                </div>

                <div class="form-group">
                    <label for="nombres">Nombres: </label><br>
                    <input name="nombres" class="form-control" required id="nombres" aria-describedby="nombresHelp" />
                    <br>
                    <div class="form-group">
                        <label for="apellido1">Apellido1: </label><br>
                        <input name="apellido1" class="form-control" required id="apellido1" aria-describedby="apellido1Help" />
                        <br>
                        <div class="form-group">
                            <label for="apellido2">Apellido2: </label><br>
                            <input name="apellido2" class="form-control" required id="apellido2" aria-describedby="apellido2Help" />
                            <br>
                            <div class="form-group">
                                <label for="telefono">Telefono: </label><br>
                                <input name="telefono" class="form-control" required id="telefono" aria-describedby="telefonoHelp" />       
                            </div>
                            <br>
                            <button type="submit" type="submit" name="accion" value="grabarCrear" class="btn btn-success">Grabar</button>
                            <button type="submit" class="btn btn-success">Salir</button>


                            </form>

                            </main>

                            </body>
                            </html>
