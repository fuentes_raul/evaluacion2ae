<%-- 
    Document   : lista
    Created on : 19-04-2020, 13:43:08
    Author     : ANDRES
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.model.entities.MasterPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<MasterPersona> personas = (List<MasterPersona>) request.getAttribute("personas");
    Iterator<MasterPersona> itPersonas = personas.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body class="text-center"> 
        <form name="form" action="controllerListado" method="POST">
               
        <h2>Lista de Pacientes:</h2>
        <a href="index.jsp">Salir</a>
        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column"><br>

            <table border="1">

                <thead>
                <th>Identificacion</th>
                <th>Nombres</th>
                <th>Apellido1</th>
                <th>Apellido2</th>
                <th>Telefono</th>

                <th></th>

                </thead>
                <tbody>

                    <%while (itPersonas.hasNext()) {
                            MasterPersona per = itPersonas.next();%>
                    <tr>
                        <td><%=per.getPerIdentificacion()%></td>
                        <td><%=per.getPerNombres()%></td>
                        <td><%=per.getPerApellido1()%></td>
                        <td><%=per.getPerApellido2()%></td>
                        <td><%=per.getTelefono()%></td>
                        <td> <input type="radio" name="seleccion" value="<%=per.getPerIdentificacion()%>"></td>
                    </tr>  
                    <%}%>
                </tbody>
            </table>
            <button type="submit" name="accion" value="editar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Editar</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Eliminar</button>
            <button type="submit" name="accion" value="crear" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Crear</button>
   </form>
        </body>
</html>


