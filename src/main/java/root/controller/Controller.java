/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.MasterPersonaDAO;
import root.model.entities.MasterPersona;

/**
 *
 * @author ANDRES
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String identificacion = request.getParameter("identificacion");
        String nombres = request.getParameter("nombres");
        String apellido1 = request.getParameter("apellido1");
        String apellido2 = request.getParameter("apellido2");
        String telefono = request.getParameter("telefono");

        MasterPersona persona = new MasterPersona();
        persona.setPerIdentificacion(identificacion);
        persona.setPerNombres(nombres);
        persona.setPerApellido1(apellido1);
        persona.setPerApellido2(apellido2);
        persona.setTelefono(telefono);

        MasterPersonaDAO dao = new MasterPersonaDAO();
        try {
            dao.create(persona);
            Logger.getLogger("log").log(Level.INFO, "valor identificacion vehiculo:{0}", persona.getPerIdentificacion());
        } catch (Exception ex) {
            Logger.getLogger("log").log(Level.SEVERE, "Se presenta un error al ingresar una persona.{0}", ex.getMessage());
        }

        request.getRequestDispatcher("salida.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MasterPersonaDAO dao = new MasterPersonaDAO();
        List<MasterPersona> personas = dao.findMasterPersonaEntities();
        System.out.println("Cantidad personas en BD"+personas.size());
        request.setAttribute("personas", personas);
        request.getRequestDispatcher("salida.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
