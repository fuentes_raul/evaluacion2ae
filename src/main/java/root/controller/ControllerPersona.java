/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.MasterPersonaDAO;
import root.model.entities.MasterPersona;

/**
 *
 * @author ANDRES
 */
@WebServlet(name = "ControllerPersona", urlPatterns = {"/controllerPersona"})
public class ControllerPersona extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerPersona</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerPersona at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String identificacion = request.getParameter("identificacion");
        String nombres = request.getParameter("nombres");
        String apellido1 = request.getParameter("apellido1");
        String apellido2 = request.getParameter("apellido2");
        String telefono = request.getParameter("telefono");
        String accion = request.getParameter("accion");

        System.out.println("identificacion" + identificacion);
        System.out.println("nombres" + nombres);
        System.out.println("apellido1" + apellido1);
        System.out.println("apellido2" + apellido2);
        System.out.println("telefono" + telefono);
        if (accion.equalsIgnoreCase("grabarCrear")) {

            try {
                MasterPersonaDAO dao = new MasterPersonaDAO();

                MasterPersona per = new MasterPersona();
                per.setPerIdentificacion(identificacion);
                per.setPerNombres(nombres);
                per.setPerApellido1(apellido1);
                per.setPerApellido2(apellido2);
                per.setTelefono(telefono);
                dao.create(per);

                List<MasterPersona> personas = dao.findMasterPersonaEntities();
                System.out.println("Cantidad personas en BD" + personas.size());

                request.setAttribute("personas", personas);
                request.getRequestDispatcher("salida.jsp").forward(request, response);

            } catch (Exception e) {
                Logger.getLogger(ControllerPersona.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        if (accion.equalsIgnoreCase("grabarEditar")) {
            try {
                MasterPersonaDAO dao = new MasterPersonaDAO();

                MasterPersona per = new MasterPersona();
                per.setPerIdentificacion(identificacion);
                per.setPerNombres(nombres);
                per.setPerApellido1(apellido1);
                per.setPerApellido2(apellido2);
                per.setTelefono(telefono);
                dao.edit(per);

                List<MasterPersona> personas = dao.findMasterPersonaEntities();
                System.out.println("Cantidad personas en BD" + personas.size());

                request.setAttribute("personas", personas);
                request.getRequestDispatcher("salida.jsp").forward(request, response);

            } catch (Exception e) {
                Logger.getLogger(ControllerPersona.class.getName()).log(Level.SEVERE, null, e);
            }

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
