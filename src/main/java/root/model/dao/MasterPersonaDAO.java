/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.dao.exceptions.PreexistingEntityException;
import root.model.entities.MasterPersona;

/**
 *
 * @author ANDRES
 */
public class MasterPersonaDAO implements Serializable {

    public MasterPersonaDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public MasterPersonaDAO() {
    }

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MasterPersona masterPersona) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(masterPersona);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMasterPersona(masterPersona.getPerIdentificacion()) != null) {
                throw new PreexistingEntityException("MasterPersona " + masterPersona + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MasterPersona masterPersona) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            masterPersona = em.merge(masterPersona);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = masterPersona.getPerIdentificacion();
                if (findMasterPersona(id) == null) {
                    throw new NonexistentEntityException("The masterPersona with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MasterPersona masterPersona;
            try {
                masterPersona = em.getReference(MasterPersona.class, id);
                masterPersona.getPerIdentificacion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The masterPersona with id " + id + " no longer exists.", enfe);
            }
            em.remove(masterPersona);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MasterPersona> findMasterPersonaEntities() {
        return findMasterPersonaEntities(true, -1, -1);
    }

    public List<MasterPersona> findMasterPersonaEntities(int maxResults, int firstResult) {
        return findMasterPersonaEntities(false, maxResults, firstResult);
    }

    private List<MasterPersona> findMasterPersonaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MasterPersona.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MasterPersona findMasterPersona(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MasterPersona.class, id);
        } finally {
            em.close();
        }
    }

    public int getMasterPersonaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MasterPersona> rt = cq.from(MasterPersona.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
