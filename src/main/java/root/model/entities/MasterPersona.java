/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ANDRES
 */
@Entity
@Table(name = "master_personas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MasterPersona.findAll", query = "SELECT m FROM MasterPersona m"),
    @NamedQuery(name = "MasterPersona.findByPerIdentificacion", query = "SELECT m FROM MasterPersona m WHERE m.perIdentificacion = :perIdentificacion"),
    @NamedQuery(name = "MasterPersona.findByPerNombres", query = "SELECT m FROM MasterPersona m WHERE m.perNombres = :perNombres"),
    @NamedQuery(name = "MasterPersona.findByPerApellido1", query = "SELECT m FROM MasterPersona m WHERE m.perApellido1 = :perApellido1"),
    @NamedQuery(name = "MasterPersona.findByPerApellido2", query = "SELECT m FROM MasterPersona m WHERE m.perApellido2 = :perApellido2"),
    @NamedQuery(name = "MasterPersona.findByTelefono", query = "SELECT m FROM MasterPersona m WHERE m.telefono = :telefono")})
public class MasterPersona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "per_identificacion")
    private String perIdentificacion;
    @Size(max = 2147483647)
    @Column(name = "per_nombres")
    private String perNombres;
    @Size(max = 2147483647)
    @Column(name = "per_apellido1")
    private String perApellido1;
    @Size(max = 2147483647)
    @Column(name = "per_apellido2")
    private String perApellido2;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;

    public MasterPersona() {
    }

    public MasterPersona(String perIdentificacion) {
        this.perIdentificacion = perIdentificacion;
    }

    public String getPerIdentificacion() {
        return perIdentificacion;
    }

    public void setPerIdentificacion(String perIdentificacion) {
        this.perIdentificacion = perIdentificacion;
    }

    public String getPerNombres() {
        return perNombres;
    }

    public void setPerNombres(String perNombres) {
        this.perNombres = perNombres;
    }

    public String getPerApellido1() {
        return perApellido1;
    }

    public void setPerApellido1(String perApellido1) {
        this.perApellido1 = perApellido1;
    }

    public String getPerApellido2() {
        return perApellido2;
    }

    public void setPerApellido2(String perApellido2) {
        this.perApellido2 = perApellido2;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perIdentificacion != null ? perIdentificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MasterPersona)) {
            return false;
        }
        MasterPersona other = (MasterPersona) object;
        if ((this.perIdentificacion == null && other.perIdentificacion != null) || (this.perIdentificacion != null && !this.perIdentificacion.equals(other.perIdentificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.MasterPersona[ perIdentificacion=" + perIdentificacion + " ]";
    }

       
}
