<%-- 
    Document   : editar
    Created on : 19-04-2020, 18:48:25
    Author     : ANDRES
--%>

<%@page import="root.model.entities.MasterPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    MasterPersona personas = (MasterPersona) request.getAttribute("personas");

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body  class="text-center">
        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            <div class="inner">

            </div>
        </header> 
    </div>   
    <h2>Editar:</h2>
    
<p class="">

<form name="form" action="controllerPersona" method="POST">
    <div class="form-group">
        <label for="identificacion">Identificacion: </label><br>
        <input name="identificacion" value="<%=personas.getPerIdentificacion()%>" class="form-control" required id="identificacion"      />
    </div>

    <div class="form-group">
        <label for="nombres">Nombres:</label><br>
        <input atep="any" name="nombres" value="<%=personas.getPerNombres()%>" class="form-control" required id="nombres"      />
        <br>
        <div class="form-group">
            <label for="apellido1">Apellido1:</label><br>
            <input name="apellido1" class="form-control" value="<%=personas.getPerApellido1()%>" class="form-control" required id="apellido1"      />
            <br>
            <div class="form-group">
                <label for="apellido2">Apellido2:</label><br>
                <input name="apellido2" class="form-control" value="<%=personas.getPerApellido2()%>" class="form-control" required id="apellido2"      />
                <br>
                <div class="form-group">
                    <label for="telefono">Telefono:</label><br>
                    <input name="telefono" class="form-control" value="<%=personas.getTelefono()%>" class="form-control" required id="telefono"      />        

                </div>
                <br>
                <button type="submit" name="accion" value="grabarEditar" class="btn btn-success">Grabar</button>
                <button type="submit" name="accion" value="SalirEditar" class="btn btn-success">Salir</button>


                </form>

            </main>

        </body>
  </html>
